import pymorphy2
import hashlib, struct
import re
import pandas as pd, numpy
import collections


def read_stop_words(filename='stopwords-ru.txt', encoding='utf-8'):
    stopwords = []
    with open(filename, encoding=encoding) as stopwords_file:
        for stopwordline in stopwords_file:
            stopwords.append(stopwordline.strip())
    return stopwords


def canonize_text(text):
    morph = pymorphy2.MorphAnalyzer()
    result_list = []
    reg = re.compile('[^а-яА-Я ]')
    text = reg.sub('', text)
    for word in text.lower().split():
        result_list.append(morph.parse(word)[0].normal_form)
    result = [element for element in result_list if element not in STOPWORDS_SET]
    #print(compute_tf(result))
    return result


def compare_by_kw(src_token_list, db_string):
    db_token_list = sorted(db_string.split(','))
    ref_len = len(db_token_list)
    intersect = [value for value in src_token_list if value in db_token_list] 
    len_inter = len(intersect)
    weight = len_inter / ref_len
    return weight


def list_digest(strings):
    hash = hashlib.sha1()
    for s in strings:
        s = s.encode('utf-8')
        hash.update(struct.pack("I", len(s)))
        hash.update(s)
    return hash.hexdigest()


def compute_tf(text):
    tf_text = collections.Counter(text)

    for i in tf_text:
        tf_text[i] = tf_text[i]/float(len(text))
    return tf_text


STOPWORDS_SET = read_stop_words()

if __name__ == '__main__':
    read_stop_words()