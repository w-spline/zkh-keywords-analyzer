from app import db

class Answers(db.Model):

  # table name
  __tablename__ = 'answers'

  id = db.Column(db.Integer, primary_key=True)
  hash = db.Column(db.String(128), unique=True, nullable=False, index=True)
  keywords = db.Column(db.Text(), nullable=False, unique=True, index=True)
  answer = db.Column(db.Text(), nullable=False)

  # class constructor
  def __init__(self, data):
    self.id = data.get('id')
    self.hash = data.get('hash')
    self.keywords = data.get('keywords')
    self.answer = data.get('answer')

  def save(self):
    db.session.add(self)
    db.session.commit()

  def update(self, data):
    for key, item in data.items():
      setattr(self, key, item)
    db.session.commit()

  def delete(self):
    db.session.delete(self)
    db.session.commit()

  @staticmethod
  def get_all_faqs():
    return Answers.query.all()

  @staticmethod
  def get_one_faq(id):
    return Answers.query.get(id)

  def __repr(self):
    return '<hash {}>'.format(self.hash)
