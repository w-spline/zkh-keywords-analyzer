from flask import Flask, Response
from flask_cors import CORS
from flask_script import Manager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate, MigrateCommand
from .config import app_config

app = Flask(__name__)
app.config.from_object(app_config['dev'])
db = SQLAlchemy(app)
migrate = Migrate(app, db)

# enable CORS
CORS(app)

from app import routes, models