from flask import jsonify, request
from app import app, db
from kws import kws
import pandas as pd, numpy


@app.route('/')
@app.route('/index')
def index():
    return 'w-spline project ready!'


@app.route('/api/v1/get_answers', methods=['GET'])
def get_answers():
    data = request.get_json()
    text = data['msg']
    parsed = kws.canonize_text(text)
    query  = "select id, keywords, answer from answers"
    dbdate = pd.read_sql_query(query, con=db.engine)
    dbdate['weight'] = dbdate.apply(lambda row: kws.compare_by_kw(parsed, row['keywords']), axis=1)
    df = dbdate.nlargest(5, 'weight')
    resdf = df.loc[df['weight'] > 0.3]
    return resdf.to_json(orient='records')