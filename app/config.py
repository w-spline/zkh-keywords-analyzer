import os

class Development(object):
    """
    Development environment configuration
    """
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'postgres://postgres:315920@localhost:5432/wspline'
    SQLALCHEMY_TRACK_MODIFICATIONS = False

class Production(object):
    """
    Production environment configurations
    """
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

app_config = {
    'dev': Development,
    'prod': Production,
}